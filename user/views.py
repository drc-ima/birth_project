import json
import os
import requests

from django.contrib.auth import authenticate, login
from django.core.mail import EmailMessage
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework import permissions

from birth. models import *
from birth.serializers import CenterSerializer
from user.serializers import *
from user.models import *
from rest_framework.status import *
from user.user_permissions import *


class AllUserList(ListAPIView):
    """
    Displays list of all users
    """
    permission_classes = (IsSuperUserPermissions, )
    serializer_class = UserSerializer
    queryset = BRUser.objects.all()


class CenterAdmins(APIView):
    """
    Displays a list of all users with center objects
    """
    permission_classes = (RegistrarUserPermissions,)

    def get(self, request):
        results = BRUser.objects.filter(user_type='CAU')
        serializer = CenterUserSerializer(results, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)


class CenterAdminsSearch(APIView):
    permission_classes = (RegistrarUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = BRUser.objects.filter(user_type='CAU')
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(position__icontains=term)
                                         | Q(department__icontains=term)
                                         | Q(branch__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(user_id__icontains=term)
                                         )
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=HTTP_400_BAD_REQUEST)
                serializer = CenterUserSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=HTTP_400_BAD_REQUEST)


class RegistrarAdmins(APIView):
    """
    Displays a list of all registrar sub admins
    """
    permission_classes = (RegistrarSuperUserPermissions,)

    def get(self, request):
        results = BRUser.objects.filter(user_type='RU')
        serializer = RegistrarUserSerializer(results, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)


class RegistrarAdminsSearch(APIView):
    permission_classes = (RegistrarSuperUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = BRUser.objects.filter(user_type='RU')
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(position__icontains=term)
                                         | Q(department__icontains=term)
                                         | Q(branch__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(user_id__icontains=term))
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=HTTP_400_BAD_REQUEST)
                serializer = RegistrarUserSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=HTTP_400_BAD_REQUEST)


class CenterSubAdminsAnnex(APIView):
    """
    Displays a list of all subs of a hospital
    """
    permission_classes = (CenterAdminUserPermissions,)

    def get(self, request):
        if not request.user.centers.all():
            return Response({'detail': 'This user has no Center Created', 'response_code': '101'},
                            status=HTTP_400_BAD_REQUEST)
        results = BRUser.objects.filter(center_id=request.user.center_id).exclude(user_type='CAU')
        serializer = CenterUserSerializer(results, context={'request': request}, many=True)
        return Response({'result': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)


class CenterSubAdminsSearch(APIView):
    permission_classes = (CenterAdminUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = BRUser.objects.filter(center_id=request.user.center_id).exclude(user_type='CAU')
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(position__icontains=term)
                                         | Q(department__icontains=term)
                                         | Q(branch__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(user_id__icontains=term))
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=HTTP_400_BAD_REQUEST)
                serializer = CenterUserSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=HTTP_400_BAD_REQUEST)


class UserList(ListAPIView):
    """
    Displays list of users of a center
    """
    permission_classes = (CenterAdminUserPermissions, )
    serializer_class = UserSerializer
    queryset = BRUser.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = BRUser.objects.filter(center_id=center_id)
        serializer = CenterUserSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=HTTP_200_OK)


class CenterSignUp(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        # date_of_birth = request.data.get('date_of_birth', None)
        email = request.data.get('email', None)
        gender = request.data.get('gender', None)
        mypassword = request.data.get('password', None)
        center_name = request.data.get('center_name', None)
        center_type = request.data.get('center_type', None)
        physical_address = request.data.get('physical_address', None)
        region = request.data.get('region', None)
        district = request.data.get('district', None)
        registration_number = request.data.get('registration_number', None)
        center_logo = request.data.get('center_logo', None)

        try:
            bruser = BRUser.objects.create(
                email=email,
                username=email,
                last_name=last_name,
                first_name=first_name,
                # date_of_birth=date_of_birth,
                sex=gender,
                is_active=False,
                user_type='CAU'
            )

            bruser.set_password(mypassword)
            bruser.save()

            center = MedicalCenter.objects.create(
                center_name=center_name,
                center_type=center_type,
                center_logo=center_logo,
                physical_address=physical_address,
                region=region,
                district=district,
                registration_number=registration_number,
                created_by=bruser,
            )
            bruser.center_id = center.id
            bruser.center_name = center.center_name
            bruser.save()
            bruser = CenterUserSerializer(bruser, context={'request': request})
            rcenter = CenterSerializer(center, context={'request': request})
            return Response({'user': bruser.data, 'center': rcenter.data, 'response_code': '100'},
                            status=HTTP_201_CREATED)
        except ValueError:
            return Response({'detail': "Email Already exist", 'response_code': '101'}, status=HTTP_400_BAD_REQUEST)


def generate_password():
    FROM = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    LENGTH = 7
    password = ""
    for i in range(LENGTH):
        password += FROM[randrange(0, len(FROM))]
    return f'{password}'


def send_email(email, content):
    email = EmailMessage('New User', content, from_email='rax.cubi@gmail.com', to=[email,], reply_to=['rax.cubi@gmail.com'])
    email.content_subtype = "html"
    email.send()


class CreateCenterUser(APIView):
    permission_classes = (CenterAdminUserPermissions,)

    def post(self, request, *args, **kwargs):
        email = request.data.get('email', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        department = request.data.get('department', None)
        position = request.data.get('position', None)
        center_id = self.request.user.center_id
        center_name = self.request.user.center_id

        try:
            bruser = BRUser.objects.create(
                email=email,
                username=email,
                last_name=last_name,
                first_name=first_name,
                department=department,
                position=position,
                center_id=center_id,
                center_name=center_name,
                is_active=True,
                user_type='CNU'
            )
            password = 'kpokekebeats'
            bruser.set_password(password)
            bruser.save()
            # center = MedicalCenter.objects.all()
            # for center in center:
            #     if center.id == center_id:
            #         message = f'<p> Hi {bruser.get_full_name()}, You have been added as a user on Birth ' \
            #                   f'Register by {center.center_name} as a {bruser.get_user_type_display()}. ' \
            #                   f'Your login details are email: {email}, ' \
            #                   f'password: {password}. After logging in, please change your password.'
            #         send_email(bruser.email, message)
            bruser = CenterUserSerializer(bruser, context={'request': request})
            return Response({'results': bruser.data, 'response_code': '100'}, status=HTTP_201_CREATED)
        except ValueError:
            return Response({'detail': 'Email Already Exist', 'response_code': '101'}, status=HTTP_400_BAD_REQUEST)


class CreateRegistrarUser(APIView):
    permission_classes = (RegistrarSuperUserPermissions,)

    def post(self, request, *args, **kwargs):
        email = request.data.get('email', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        region = request.data.get('region', None)
        branch = request.data.get('branch', None)
        position = request.data.get('position', None)

        try:
            bruser = BRUser.objects.create(
                email=email,
                username=email,
                last_name=last_name,
                first_name=first_name,
                region=region,
                branch=branch,
                position=position,
                is_active=True,
                user_type='RU'
            )
            password = 'kpokekebeats'
            bruser.set_password(password)
            bruser.save()
            # message = f'<p> Hi {bruser.get_full_name()}, You have been added as a user on Birth ' \
            #           f'Register  as a {bruser.get_user_type_display()}. ' \
            #           f'Your login details are email: {email}, ' \
            #           f'password: {password}. After logging in, please change your password.'
            # send_email(bruser.email, message)
            bruser = RegistrarUserSerializer(bruser, context={'request': request})
            return Response({'results': bruser.data, 'response_code': '100'}, status=HTTP_201_CREATED)
        except ValueError:
            return Response({'detail': 'Email Already Exist', 'response_code': '101'}, status=HTTP_400_BAD_REQUEST)


class UserSignIn(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        email = request.data.get('email')
        password = request.data.get('password')

        if not email:
            return Response({'detail': 'Please provide an email', 'response_code': '101'}, status=HTTP_400_BAD_REQUEST)

        if not password:
            return Response({'detail': 'Please provide a password', 'response_code': '101'},
                            status=HTTP_400_BAD_REQUEST)

        try:
            user = authenticate(request, username=email, password=password)

            if user is not None and user.is_active:
                login(request, user)
                url = f'http://{request.get_host()}/auth/token/'
                payload = {
                    'username': email,
                    'password': password,
                    'grant_type': 'password',
                }
                headers = {'content-type': 'application/x-www-form-urlencoded',
                           'Authorization': f'JWT {os.environ.get("BASE_64_MASHUP")}'}
                myrequest = requests.post(url, payload, headers=headers)
                user.login_number += 1
                user.save()
                return Response({'results': {
                    'token': json.loads(myrequest.text),
                    'id': user.user_id,
                    'full_name': user.get_full_name(),
                    'username': user.username,
                    'user_type': user.get_user_type_display(),
                    'login_number': user.login_number,
                },
                    'response_code': '100'}, status=HTTP_200_OK)
            else:
                return Response({'detail': 'Credentials do not much or user is not active', 'response_code': '101'},
                                status=HTTP_400_BAD_REQUEST)
        except BRUser.DoesNotExist:
            return Response({'detail': 'Email does not exist', 'response_code': '101'}, status=HTTP_400_BAD_REQUEST)


class ActivateUser(APIView):
    permission_classes = (RegistrarSuperUserPermissions,)

    def post(self, request):
        user_id = request.data.get('user_id', None)

        try:
            user = BRUser.objects.get(user_id=user_id)
            if not user.is_active:
                user.is_active = True
                user.save()
                return Response({'results': 'User Account is now active', 'response_code': '100'}, status=HTTP_200_OK)
            else:
                return Response({'detail': 'User account is already active', 'response_code': '101'},
                                status=HTTP_400_BAD_REQUEST)
        except:
            return Response({'detail': 'User not found', 'response_code': '101'}, status=HTTP_400_BAD_REQUEST)


class UserDetail(RetrieveAPIView):
    permission_classes = (GeneralUserPermissions,)
    serializer_class = UserSerializer
    lookup_field = 'user_id'
    queryset = BRUser.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
