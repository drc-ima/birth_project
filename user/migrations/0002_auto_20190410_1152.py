# Generated by Django 2.2 on 2019-04-10 11:52

from django.db import migrations, models
import user.models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bruser',
            name='user_id',
            field=models.CharField(default=user.models.generate_user_id, max_length=10, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='bruser',
            name='user_type',
            field=models.CharField(blank=True, choices=[('CAU', 'Center Admin User'), ('WNU', 'Web Normal User'), ('RU', 'Registrar User'), ('RSU', 'Registrar Super User'), ('CNU', 'Center Nurse User')], default='WNU', max_length=255, null=True, verbose_name='User Type'),
        ),
    ]
