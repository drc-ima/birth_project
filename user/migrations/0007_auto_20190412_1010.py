# Generated by Django 2.2 on 2019-04-12 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0006_auto_20190412_0000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bruser',
            name='user_type',
            field=models.CharField(blank=True, choices=[('RU', 'Registrar User'), ('CNU', 'Center Nurse User'), ('CAU', 'Center Admin User'), ('WNU', 'Web Normal User'), ('RSU', 'Registrar Super User')], default='WNU', max_length=255, null=True, verbose_name='User Type'),
        ),
    ]
