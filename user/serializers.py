from rest_framework import serializers

from .models import *


class UserSerializer(serializers.ModelSerializer):
    full_name = serializers.URLField(source='get_full_name')
    user_type_display = serializers.SerializerMethodField()

    class Meta:
        model = BRUser
        fields = (
            'user_id', 'username', 'email', 'first_name', 'last_name', 'center_id', 'center_name', 'sex',
            'date_of_birth', 'is_active', 'user_type', 'user_type_display', 'date_joined', 'last_login', 'full_name'
        )

    def get_user_type_display(self, obj):
        return obj.get_user_type_display()


class CenterUserSerializer(serializers.ModelSerializer):
    full_name = serializers.URLField(source='get_full_name')
    user_type_display = serializers.SerializerMethodField()

    class Meta:
        model = BRUser
        fields = (
            'user_id', 'username', 'email', 'first_name', 'last_name', 'center_id', 'center_name', 'sex', 'department', 'position',
            'date_of_birth', 'is_active', 'user_type', 'user_type_display', 'date_joined', 'last_login', 'full_name'
        )

    def get_user_type_display(self, obj):
        return obj.get_user_type_display()


class RegistrarUserSerializer(serializers.ModelSerializer):
    full_name = serializers.URLField(source='get_full_name')
    user_type_display = serializers.SerializerMethodField()

    class Meta:
        model = BRUser
        fields = (
            'user_id', 'username', 'email', 'first_name', 'last_name', 'sex',
            'date_of_birth', 'is_active', 'user_type', 'user_type_display', 'date_joined', 'last_login', 'full_name'
        )

    def get_user_type_display(self, obj):
        return obj.get_user_type_display()