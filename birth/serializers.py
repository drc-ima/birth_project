from rest_framework import serializers
from .models import *


class BirthSerializer(serializers.ModelSerializer):
    created_by_center = serializers.ReadOnlyField(source='created_by_center.center_name')
    created_by = serializers.ReadOnlyField(source='created_by.get_full_name')
    parents1 = serializers.ReadOnlyField(source='parents1.get_full_name')
    parents2 = serializers.ReadOnlyField(source='parents2.get_full_name')
    full_name = serializers.ReadOnlyField(source='get_full_name')

    class Meta:
        model = Birth
        fields = ('id', 'first_name', 'last_name', 'full_name', 'sex', 'ward', 'date_of_birth', 'born_at', 'parents1',
                  'parents2',
                  'doctor_name', 'g6pd_status', 'is_sickle_cell', 'birth_weight', 'created_by_center', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by_center', 'created_by', 'created_at')

    # def get_sex_display(self, obj):
    #     return obj.get_sex_display()

    # def get_parents1_id_display(self, obj):
    #     return obj.get_parent1_display()
    #
    # def get_parents2_id_display(self, obj):
    #     return obj.get_parent2_display()


class DoctorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Doctor
        fields = ('id', 'full_name', 'department', 'employee_number', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by', 'created_at')


class GuardianSerializer(serializers.ModelSerializer):
    created_by_center = serializers.ReadOnlyField(source='created_by_center.center_name')
    created_by = serializers.ReadOnlyField(source='created_by.get_full_name')
    full_name = serializers.ReadOnlyField(source='get_full_name')

    class Meta:
        model = Guardian
        fields = ('id', 'guardian', 'first_name', 'last_name', 'other_name', 'full_name', 'date_of_birth', 'number_of_children', 'nationality', 'religion', 'contact_info', 'region', 'level_of_education', 'town_or_village', 'home_address', 'created_by_center', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by', 'created_by_center', 'created_at')


class CenterSerializer(serializers.ModelSerializer):
    center_type_display = serializers.SerializerMethodField
    created_by = serializers.ReadOnlyField(source='created_by.get_full_name')

    class Meta:
        model = MedicalCenter
        fields = ('id', 'center_name', 'center_type', 'physical_address', 'registration_number', 'region', 'district', 'center_logo', 'created_by', 'created_at')
        read_only_fields = ('id', 'created_by', 'created_at')

    def get_center_type_display(self, obj):
        return obj.get_center_type_display()
