from django.db.models import Q
from rest_framework.views import *
from rest_framework.generics import *
from rest_framework.permissions import *

from user.models import BRUser
from user.user_permissions import *
from .serializers import *
from .models import *


class CenterList(ListAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = CenterSerializer
    queryset = MedicalCenter.objects.all()


class CenterDetail(RetrieveAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = CenterSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return MedicalCenter.objects.filter(created_by=self.request.user, id=self.request.user.center_id)

    def get(self, request, *args, **kwargs):
        return self.retrieve(self, request, *args, **kwargs)


class CenterUpdate(UpdateAPIView):
    permission_classes = (CenterAdminUserPermissions,)
    serializer_class = CenterSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return MedicalCenter.objects.filter(created_by=self.request.user)


class CenterGuardianSearch(APIView):
    permission_classes = (CenterUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            center_id = self.request.user.center_id
            results = Guardian.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(other_name__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(town_or_village__icontains=term)
                                         | Q(nationality__icontains=term))
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = GuardianSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class GuardianSearch(APIView):
    permission_classes = (RegistrarUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = Guardian.objects.all()
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(other_name__icontains=term)
                                         | Q(region__icontains=term)
                                         | Q(town_or_village__icontains=term)
                                         | Q(nationality__icontains=term))
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = GuardianSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class CenterBirthSearch(APIView):
    permission_classes = (CenterUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            center_id = self.request.user.center_id
            results = Birth.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(id__icontains=term)
                                         | Q(born_at__icontains=term)
                                         )
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class BirthSearch(APIView):
    permission_classes = (RegistrarUserPermissions, )

    def post(self, request):
        search = request.data.get('search', None)

        if search is not None:
            results = Birth.objects.all()
            for term in search.split():
                results = results.filter(Q(first_name__icontains=term)
                                         | Q(last_name__icontains=term)
                                         | Q(id__icontains=term)
                                         | Q(born_at__icontains=term)
                                         )
                if not results:
                    return Response({'results': 'Your search was not found', 'response_code': '101'},
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = BirthSerializer(results, context={'request': request}, many=True)
                return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
            return Response({'detail': 'Search Text is empty', 'response_code': '102'},
                            status=status.HTTP_400_BAD_REQUEST)


class CreateCenterGuardian(CreateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def create(self, request, *args, **kwargs):
        guardian = request.data.get('guardian', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        other_name = request.data.get('other_name', None)
        date_of_birth = request.data.get('date_of_birth', None)
        children = request.data.get('number_of_children', None)
        nationality = request.data.get('nationality', None)
        religion = request.data.get('religion', None)
        contact_info = request.data.get('contact_info', None)
        home_address = request.data.get('home_address', None)
        town_or_village = request.data.get('town_or_village', None)
        region = request.data.get('region', None)
        level_of_education = request.data.get('level_of_education', None)
        center_id = self.request.user.center_id

        guardian = Guardian.objects.create(
            guardian=guardian,
            first_name=first_name,
            last_name=last_name,
            other_name=other_name,
            date_of_birth=date_of_birth,
            number_of_children=children,
            nationality=nationality,
            religion=religion,
            contact_info=contact_info,
            home_address=home_address,
            town_or_village=town_or_village,
            region=region,
            level_of_education=level_of_education,
            created_by_center=MedicalCenter.objects.get(id=center_id)
        )
        serializer = GuardianSerializer(guardian, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_201_CREATED)


class CreateRegistrarGuardian(CreateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def create(self, request, *args, **kwargs):
        guardian = request.data.get('guardian', None)
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        other_name = request.data.get('other_name', None)
        date_of_birth = request.data.get('date_of_birth', None)
        children = request.data.get('number_of_children', None)
        nationality = request.data.get('nationality', None)
        religion = request.data.get('religion', None)
        contact_info = request.data.get('contact_info', None)
        home_address = request.data.get('home_address', None)
        town_or_village = request.data.get('town_or_village', None)
        region = request.data.get('region', None)
        level_of_education = request.data.get('level_of_education', None)

        guardian = Guardian.objects.create(
            guardian=guardian,
            first_name=first_name,
            last_name=last_name,
            other_name=other_name,
            date_of_birth=date_of_birth,
            number_of_children=children,
            nationality=nationality,
            religion=religion,
            contact_info=contact_info,
            home_address=home_address,
            town_or_village=town_or_village,
            region=region,
            level_of_education=level_of_education,
            created_by=BRUser.objects.get(user_id=self.request.user.user_id)
        )
        serializer = GuardianSerializer(guardian, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_201_CREATED)


class CenterGuardianList(ListAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = Guardian.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
        serializer = GuardianSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class AllGuardianList(ListAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = GuardianSerializer
    queryset = Guardian.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = Guardian.objects.all()
        serializer = GuardianSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class CenterGuardianUpdate(UpdateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = GuardianSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Guardian.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class RegistrarGuardianUpdate(UpdateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = GuardianSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return Guardian.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class CreateDoctor(CreateAPIView):
    permission_classes = (IsAuthenticated, NurseUserPermissions, CenterAdminUserPermissions)
    serializer_class = DoctorSerializer
    queryset = Doctor.objects.all()

    def create(self, request, *args, **kwargs):
        full_name = request.data.get('full_name', None)
        department = request.data.get('department', None)
        employee_number = request.data.get('employee_number', None)
        center_id = self.request.user.center_id

        doctor = Doctor.objects.create(
            full_name=full_name,
            department=department,
            employee_number=employee_number,
            created_by=MedicalCenter.objects.get(id=center_id)
        )
        serializer = DoctorSerializer(doctor, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': 100}, status=status.HTTP_201_CREATED)


class DoctorList(ListAPIView):
    permission_classes = (IsAuthenticated, CenterAdminUserPermissions)
    serializer_class = DoctorSerializer
    queryset = Doctor.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = Doctor.objects.filter(created_by__is_active=True, created_by__id=center_id)
        serializer = DoctorSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class DoctorUpdate(UpdateAPIView):
    permission_classes = (IsAuthenticated, CenterAdminUserPermissions)
    serializer_class = DoctorSerializer
    lookup_field = 'id'

    def get_queryset(self):
        center_id = self.request.user.center_id
        return Doctor.objects.filter(created_by__is_active=True, created_by__id=center_id)

    def update(self, request, *args, **kwargs):
        try:
            obj = self.get_object()
            data = request.data
            serializer = DoctorSerializer(obj, context={'request': request}, data=data, partial=True)
            if serializer.is_valid():
                serializer.save()
            return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
        except Exception:
            return Response({'detail': 'Request for not owned object', 'response_code': '101'},
                            status=status.HTTP_400_BAD_REQUEST)


class CreateCenterBirth(CreateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()

    def create(self, request, *args, **kwargs):
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        birth_weight = request.data.get('birth_weight', None)
        is_sickle_cell = request.data.get('is_sickle_cell', None)
        g6pd_status = request.data.get('g6pd_status', None)
        doctor_name = request.data.get('doctor_name', None)
        sex = request.data.get('sex', None)
        ward = request.data.get('ward', None)
        date_of_birth = request.data.get('date_of_birth', None)
        born_at = self.request.user.center_id
        parent1 = request.data.get('parent1', None)
        parent2 = request.data.get('parent2', None)
        center_id = self.request.user.center_id

        doctor = Birth.objects.create(
            first_name=first_name,
            last_name=last_name,
            birth_weight=birth_weight,
            is_sickle_cell=is_sickle_cell,
            g6pd_status=g6pd_status,
            doctor_name=doctor_name,
            parents1_id=parent1,
            parents2_id=parent2,
            sex=sex,
            ward=ward,
            date_of_birth=date_of_birth,
            born_at=MedicalCenter.objects.get(id=born_at),
            created_by_center=MedicalCenter.objects.get(id=center_id),
        )
        serializer = BirthSerializer(doctor, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': 100}, status=status.HTTP_201_CREATED)


class CreateRegistrarBirth(CreateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()

    def create(self, request, *args, **kwargs):
        first_name = request.data.get('first_name', None)
        last_name = request.data.get('last_name', None)
        birth_weight = request.data.get('birth_weight', None)
        is_sickle_cell = request.data.get('is_sickle_cell', None)
        g6pd_status = request.data.get('g6pd_status', None)
        doctor_name = request.data.get('doctor_name', None)
        sex = request.data.get('sex', None)
        ward = request.data.get('ward', None)
        date_of_birth = request.data.get('date_of_birth', None)
        born_at = request.data.get('born_at', None)
        parent1 = request.data.get('parent1', None)
        parent2 = request.data.get('parent2', None)

        doctor = Birth.objects.create(
            first_name=first_name,
            last_name=last_name,
            birth_weight=birth_weight,
            is_sickle_cell=is_sickle_cell,
            g6pd_status=g6pd_status,
            doctor_name=doctor_name,
            parents1_id=parent1,
            parents2_id=parent2,
            sex=sex,
            ward=ward,
            date_of_birth=date_of_birth,
            born_at=born_at,
            created_by=BRUser.objects.get(user_id=self.request.user.user_id),
        )
        serializer = BirthSerializer(doctor, context={'request': request}, many=False)
        return Response({'results': serializer.data, 'response_code': 100}, status=status.HTTP_201_CREATED)


class CenterBirthList(ListAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()

    def list(self, request, *args, **kwargs):
        center_id = self.request.user.center_id
        queryset = Birth.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)
        serializer = BirthSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class AllBirthList(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = Birth.objects.all()
        serializer = BirthSerializer(queryset, context={'request': request}, many=True)
        return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)


class BirthList(ListAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()


class CenterBirthDetail(RetrieveAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthSerializer
    lookup_field = 'id'

    def get_queryset(self):
        center_id = self.request.user.center_id
        return Birth.objects.filter(created_by_center__is_active=True, created_by_center__id=center_id)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class RegistrarBirthDetail(RetrieveAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthSerializer
    lookup_field = 'id'

    def get_queryset(self):
        center_id = self.request.user.center_id
        return Birth.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class CenterBirthUpdate(UpdateAPIView):
    permission_classes = (CenterUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    # def put(self, request, *args, **kwargs):
    #     try:
    #         obj = self.get_object()
    #         data = request.data
    #         serializer = BirthSerializer(obj, context={'request': request}, data=data, partial=True)
    #         if serializer.is_valid():
    #             serializer.save()
    #         return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
    #     except Exception:
    #         return Response({'detail': 'Request for not owned object', 'response_code': '101'},
    #                         status=status.HTTP_400_BAD_REQUEST)
    #
    # def patch(self, request, *args, **kwargs):
    #     try:
    #         obj = self.get_object()
    #         data = request.data
    #         serializer = BirthSerializer(obj, context={'request': request}, data=data, partial=True)
    #         if serializer.is_valid():
    #             serializer.save()
    #         return Response({'results': serializer.data, 'response_code': '100'}, status=status.HTTP_200_OK)
    #     except Exception:
    #         return Response({'detail': 'Request for not owned object', 'response_code': '101'},
    #                         status=status.HTTP_400_BAD_REQUEST)


class RegistrarBirthUpdate(UpdateAPIView):
    permission_classes = (RegistrarUserPermissions,)
    serializer_class = BirthSerializer
    queryset = Birth.objects.all()
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
