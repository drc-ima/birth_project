from django.contrib import admin
from . import models


admin.site.register(models.Birth)
admin.site.register(models.Doctor)
admin.site.register(models.Guardian)
admin.site.register(models.MedicalCenter)
