from random import randrange

from django.utils import timezone

from user.models import SEX, USER_DIR
from django.db import models
from django.conf import settings


TYPE = {
    ('HOS', 'Hospital'),
    ('CLI', 'Clinic'),
    ('OTH', 'Other')
}


def generate_center_id():
    FROM = '0123456789'
    LENGTH = 7
    center_id = ""
    for i in range(LENGTH):
        center_id += FROM[randrange(0, len(FROM))]
    return f'CNT{center_id}'


class MedicalCenter(models.Model):
    id = models.CharField(primary_key=True, default=generate_center_id, max_length=255, editable=False)
    center_name = models.CharField(max_length=255, blank=True, null=True)
    center_type = models.CharField(max_length=255, blank=True, null=True)
    registration_number = models.CharField(max_length=255, blank=True, null=True)
    physical_address = models.CharField(max_length=255, blank=True, null=True)
    region = models.CharField(max_length=255, blank=True, null=True)
    district = models.CharField(max_length=255, blank=True, null=True)
    center_logo = models.TextField(max_length=200000, blank=True, null=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='centers',
        on_delete=models.SET_NULL,
        blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.center_name}'

    class Meta:
        ordering = ('id', 'center_name',)
        verbose_name_plural = 'Medical Centers'


class Guardian(models.Model):
    guardian = models.CharField(max_length=255, blank=True, null=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    other_name = models.CharField(max_length=255, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    number_of_children = models.IntegerField(blank=True, null=True)
    nationality = models.CharField(max_length=255, blank=True, null=True)
    religion = models.CharField(max_length=255, blank=True, null=True)
    contact_info = models.CharField(max_length=255, blank=True, null=True)
    home_address = models.CharField(max_length=255, blank=True, null=True)
    town_or_village = models.CharField(max_length=255, blank=True, null=True)
    region = models.CharField(max_length=255, blank=True, null=True)
    level_of_education = models.CharField(max_length=255, blank=True, null=True)
    created_by_center = models.ForeignKey(
        MedicalCenter,
        related_name='center_guardians',
        on_delete=models.SET_NULL,
        blank=True, null=True
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='reg_guardians',
        on_delete=models.SET_NULL,
        blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    def get_full_name(self):
        return f'{self.first_name} {self.other_name} {self.last_name}'

    def __str__(self):
        return self.get_full_name()

    class Meta:
        ordering = ('id', 'created_at')


class Doctor(models.Model):
    full_name = models.CharField(max_length=255, blank=True, null=True)
    department = models.CharField(max_length=255, blank=True, null=True)
    employee_number = models.CharField(max_length=255, blank=True, null=True)
    created_by = models.ForeignKey(
        MedicalCenter,
        related_name='doctors',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('id', 'created_at')

    def __str__(self):
        return self.full_name


def generate_birth_id():
    FROM = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    LENGTH = 10
    center_id = ""
    for i in range(LENGTH):
        center_id += FROM[randrange(0, len(FROM))]
    return f'MBR{center_id}'


class Birth(models.Model):
    id = models.CharField(max_length=255, primary_key=True, default=generate_birth_id, editable=False)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    sex = models.CharField(max_length=255, blank=True, null=True)
    ward = models.CharField(max_length=255, blank=True, null=True)
    birth_weight = models.CharField(max_length=255, blank=True, null=True)
    is_sickle_cell = models.BooleanField(default=False, blank=True, null=True)
    g6pd_status = models.CharField(max_length=255, blank=True, null=True)
    date_of_birth = models.DateTimeField(default=timezone.now, blank=True, null=True)
    born_at = models.CharField(max_length=300, blank=True, null=True)
    parents1 = models.ForeignKey(Guardian, related_name='birth_parent1', on_delete=models.SET_NULL, blank=True,
                                 null=True)
    parents2 = models.ForeignKey(Guardian, related_name='birth_parent2', on_delete=models.SET_NULL, blank=True,
                                 null=True)
    doctor_name = models.CharField(max_length=300, blank=True, null=True)
    created_by_center = models.ForeignKey(
        MedicalCenter,
        related_name='center_births',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='reg_births',
        on_delete=models.SET_NULL,
        blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.id}, {self.date_of_birth}'

    class Meta:
        ordering = ('date_of_birth', 'id')
